// Express Setup
const express = require('express');
const mongoose = require('mongoose');
// Allows our backend application to be available for use in our frontend application
// Allows us to control the app's Cross-Origin Resources Sharing Settings.
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');
const app = express();
const port=process.env.PORT || 4000;


//middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


//Main Uri
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

mongoose.connect(`mongodb+srv://gabrieldguillermo:admin123@zuitt-batch-197.w7cnjly.mongodb.net/s37-s41?retryWrites=true&w=majority`,{
	useNewUrlParser : true,
	useUnifiedTopology : true
});

const db = mongoose.connection
db.on('error', () => console.error('Connection Error!'));
db.once('open', () => console.log('Connected to MongoDB!'))


app.listen(port, () => console.log(`API is now online at port ${port}`))