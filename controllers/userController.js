const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// check email if exist
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0){
			return true
		} else {
			return false
		}
	})
};


// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		 firstName: reqBody.firstName,
		 lastName: reqBody.lastName,
		 email: reqBody.email,
		 mobileNo: reqBody.mobileNo,
		 password: bcrypt.hashSync(reqBody.password, 10)
	})

		return newUser.save().then((user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
};


// login User
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			// compareSync(<dataTocompare> , <encrypted password>)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

module.exports.getProfile = (userData) => {

	return User.findById(userData.id).then(result => {
		console.log(userData)
		if (result == null) {
			return false
		} else {
		console.log(result)

		result.password = "*****";

		// Returns the user information with the password as an empty string
		return result;

		}
	});

};



//enroll user
module.exports.enroll = async (data,verifyData) => {

	if(verifyData.isAdmin){
 		return false
	} else {

		let isUserUpdated = await User.findById(verifyData.id).then(user => {

		user.enrollments.push({courseId: data})

		return user.save().then((user, error) => {

			if(error) {
				return false
			} else {
				return true
			}
		})
	})


	let isCourseUpdated = await Course.findById(data).then(course => {

		course.enrollees.push({userId: verifyData.id})

		return course.save().then((course, error) => {

			if(error) {
				return false
			} else {
				return true
			}
		})
	})


	if (isUserUpdated && isCourseUpdated) {
		return true
	} else {
		return false
	}

	}

};
