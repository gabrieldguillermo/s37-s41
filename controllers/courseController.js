const Course = require('../models/Course');
const User = require('../models/User');
// Create a new course

module.exports.addCourse = (userData, reqBody) => {

	return User.findById(userData.id).then(result => {
		console.log(userData);
		console.log(reqBody);
		if(result == null){
			return false
		} else {

			if(result.isAdmin == true){
				let newCourse = new Course({
					name: reqBody.name,
					description: reqBody.description,
					price: reqBody.price
				});

				return newCourse.save().then((course, error) => {
					if(error){
						return false
					} else {
						return true
					}
				});

			} else {
				return "Not an admin"
			}
		}
	});	
};

// Retrieve a Specific course
module.exports.getAllCourses = (userData) => {
	if(userData.isAdmin){
		return Course.find({}).then(result => {
			return result
		})
	} else {
		return false
	}
};

// Retrieve ALl active Courses
module.exports.getAllActive = () =>{
	return Course.find({isActive: true}).then( result => { 
		return result
		});
	};


// Retrieve a Specific course
module.exports.getcourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	});
};


//update a specific course
module.exports.updateCourse = (reqParams,reqBody,data) => {
	 if(data){
	 	let updatedCourse = {
	 		name: reqBody.name,
	 		description : reqBody.description,
	 		price : reqBody.price
	 	}
	 	return Course.findByIdAndUpdate( reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
	 		if(error){
	 			return false
	 		} else {
	 			return true
	 		}
	 	})
	 } else {
	 	return `You are not an ADMIN!`
	 }
};

//archive a specific course
module.exports.archiveCourse = (reqParams, reqBody, isAdmin) => {

	 if(isAdmin){
	 	let updateStatus = {
	 		isActive: reqBody.isActive
	 	}
	 	return Course.findByIdAndUpdate(reqParams.courseId, updateStatus).then((statusUpdated, error) => {
	 		if(error){
	 			return false
	 		} else {
	 			return statusUpdated
	 		}
	 	});

	 } else {
	 	return `You are not an ADMIN!`
	 }
};

