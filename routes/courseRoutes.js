const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require('../auth');


// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	let userData =  auth.decode(req.headers.authorization);
	console.log(userData)

	courseController.addCourse({id:userData.id}, req.body).then(resultFromController => {
		res.send(resultFromController)
	})
});


// Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});


// Route for retrieving specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.id)
	courseController.getcourse(req.params).then(resultFromController => res.send(resultFromController))
});

//Router for update
router.put("/:courseId", auth.verify, (req, res)=>{

	const isAdminData = auth.decode(req.headers.authorization).isAdmin;
	console.log(isAdminData)
	courseController.updateCourse(req.params,req.body, isAdminData).then( resultFromController => res.send(resultFromController));
})

router.put("/:courseId/archive", auth.verify, (req, res)=>{

	const isAdminData = auth.decode(req.headers.authorization).isAdmin;
	console.log(isAdminData)
	courseController.archiveCourse(req.params,req.body, isAdminData).then( resultFromController => res.send(resultFromController));
})


module.exports = router